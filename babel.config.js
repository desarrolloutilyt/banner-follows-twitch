module.exports = api => {
  const environment = api.env() == 'production' ? '[hash:base64]' : '[name]__[local]'
  api.cache(true);


  const config = {
    presets: ["@babel/preset-env", "@babel/preset-react"],
    plugins: [
      [
        "@babel/plugin-transform-runtime",
        {
          regenerator: true
        }
      ],
      [
        "babel-plugin-react-css-modules",
        {
          generateScopedName: environment,
          autoResolveMultipleImports: true,
          filetypes: {
            ".scss": {
              syntax: "postcss-scss",
              plugins: [["postcss-import-sync2"]]
            }
          }
        }
      ]
    ]
  };

  return config;
};
