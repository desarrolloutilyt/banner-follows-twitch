const webpack = require('webpack');
const path = require("path");
const OfflinePlugin = require('offline-plugin');

process.env.NODE_ENV = 'production';

const babelLoader = {
	loader: "babel-loader",
	options: {
		cacheDirectory: true
	}
};

const CSSLoader = {
	loader: "css-loader",
	options: {
		modules: {
			localIdentName: "[hash:base64]"
		}
	}
};

const postCSSLoader = {
	loader: "postcss-loader",
	options: {
		ident: "postcss"
	}
};

module.exports = {
	entry: {
		index: "./src/app/index.js",
		vendor: ['offline-plugin/runtime', /* … */],
	},
	output: {
		filename: "[name].bundle.js",
		path: __dirname + "/public"
	},
	module: {
		rules: [
			{
				oneOf: [
					{
						test: /\.(js|jsx|mjs)$/,
						exclude: /node_modules/,
						use: [babelLoader]
					},
					{
						test: /\.scss$/,
						exclude: /\.module\.scss$/,
						use: ["style-loader", CSSLoader, postCSSLoader, "sass-loader"]
					},

					{
						test: /\.css$/,
						use: [
							"style-loader",
							{
								loader: "css-loader",
								options: {
									importLoaders: 1
								}
							},
							{
								loader: "postcss-loader",
								options: {
									ident: "postcss",
									plugins: () => [
										require("postcss-flexbugs-fixes")
									]
								}
							}
						]
					}
				]
			}
		]
	},
	resolve: {
		alias: {
			react: path.resolve('./node_modules/react'),
			'react-router-dom': path.resolve('./node_modules/react-router-dom')
		}
	},
	plugins: [
		new webpack.optimize.ModuleConcatenationPlugin(),
		new OfflinePlugin({
			AppCache: false,
			ServiceWorker: { events: true },
		}),
	],
};
