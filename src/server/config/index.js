const express = require('express');
const routes = require('./routes');

module.exports = app => {
	const port = process.env.PORT || 3500;

	app.set("port", port);

	app.use(express.urlencoded({ extended: false }));
	app.use(express.json());

	routes(app);

	return app;
};