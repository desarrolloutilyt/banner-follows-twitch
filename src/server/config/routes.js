const express = require('express');
const path = require('path');

const router = express.Router();

module.exports = app => {
	const public = '../../../public';

	app.use('/', express.static(path.join(__dirname, public)));

	router.use('/', (req, res) => {
		res.sendFile(path.join(__dirname, public, 'index.html'));
	});

	app.use(router);
}