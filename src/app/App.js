import React, { useEffect, useState } from 'react';

import axios from 'axios';

import './styles/App.scss';

const App = () => {
	const [followers, setFollowers] = useState();
	const [renderFollow, setRenderFollow] = useState();

	//Primera llamada a la API
	useEffect(() => { useFollowers(followers, setFollowers) }, []);

	//Actualizar los followers llamanado a la API de Twitch
	useEffect(() => {
		const intervalUpdate = setInterval(() => {
			useFollowers(followers, setFollowers);
		}, 10000);
		return () => clearInterval(intervalUpdate)
	}, [followers]);

	//Cambiar de follower
	useEffect(() => {
		if (followers) {
			const timer = followers.map(i => i.length).reduce((a, b) => a + b) * 500 + 1000 * followers.length;

			dibujarCaracter(followers, setRenderFollow)
			const dibujarInterval = setInterval(() => {
				dibujarCaracter(followers, setRenderFollow)
			}, timer)
			return () => clearInterval(dibujarInterval)
		}

	}, [followers]);

	return (
		<div styleName='cntFollowers'>
			<div styleName='cabecera'>
				<span></span>
				<h1>ÚLTIMOS FOLLOWS</h1>
			</div>
			<div styleName='follower'>
				<p>{renderFollow}</p>
			</div>
		</div>
	)
};

//Función que pinta cada follower, caracter a caracter
const dibujarCaracter = async (followers, setRenderFollow) => {
	const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

	for (let i = 0; i < followers.length; i++) {
		let word = '';
		for (let j = 0; j < followers[i].length; j++) {
			word += followers[i][j];
			setRenderFollow(word);
			await sleep(500);
		}
		await sleep(3000);
	}
}

//Función que realiza la llamada a la API de Twitch y recupera los followers
const useFollowers = async (followers, setFollowers) => {
	const instance = await axios.create({
		timeout: 1000
	})

	let response = await instance.get('	https://api.twitch.tv/kraken/channels/480299489/follows?client_id=mxl8gqk3vfonb1b9rvhkxqdglcm2sg&api_version=5');

	const newFollowers = response.data.follows.map(i => i.user.name)

	if (!followers || followers[0] !== newFollowers[0])
		setFollowers(newFollowers)
}

export default App;